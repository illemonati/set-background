/**
 * @name SetBackground
 * @version 0.0.1
 * @description Set background
 */

const fs = require("fs");

class SetBackground {
    running = false;
    backgroundURL = "";
    start() {
        this.running = true;
        console.log("Set background started");
        const backgroundBuffer = fs.readFileSync(
            "/Users/illemonati/Pictures/green-candle/962184661889609728.IMG_2668.png"
        );
        this.backgroundURL = URL.createObjectURL(
            new Blob([backgroundBuffer], { type: "image/png" })
        );
        setInterval(() => {
            if (this.running) this.setBackground();
        }, 200);
    }
    stop() {
        this.running = false;
        URL.revokeObjectURL(this.backgroundURL);
        console.log("set background stopped");
    }
    setBackground() {
        const URLString = `url('${this.backgroundURL}')`;
        if (
            document.documentElement.style.getPropertyValue(
                "--background-image"
            ) != URLString
        ) {
            document.documentElement.style.setProperty(
                "--background-image",
                URLString
            );
        }
    }
}

module.exports = SetBackground;
